@login-dashboard
Feature: Login Dashboard MyPertamina
Scenario: Login as SuperAdmin at Dashboard MyPertamina
    When user is on MyPertamina Dashboard login page
    And user input username at username field
    And user input password at password field
    And user click masuk button
    Then user successfully logged in as username at MyPertamina Dashboard