def scroll_to(element, is_top, obj = self)
    # if is_top true >> scroll top of element to top of screen
    # if is_top false >> scroll bottom of element to bottom of screen
    script = <<-JS
    arguments[0].scrollIntoView(arguments[1]);
    JS
    obj.page.driver.browser.execute_script(script, element.native, is_top)
end

def scroll_page(horizontal, vertical, obj = self)
    obj.page.execute_script "window.scrollBy(#{horizontal}, #{vertical})"
end

def scroll_down(range, obj = self)
    scroll_page(0, range, obj)
end

def scroll_up(range, obj = self)
    scroll page(0, -range, obj)
end

def scroll_right(range, obj = self)
    scroll_page(range, 0, obj)
end

def scroll_left(range, obj = self)
    scroll_page(-range, 0, obj)
end
