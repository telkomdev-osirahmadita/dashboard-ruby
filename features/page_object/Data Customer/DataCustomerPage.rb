class DataCustomer < SitePrism::Page
    
    element :download_report_data_customer, '//*[@id="app"]/main/div[2]/div[1]/div/div/div[1]/button'
    element :search_field, '//*[@id="app"]/main/div[2]/div[1]/div/div/div[2]/div/div/input'
end