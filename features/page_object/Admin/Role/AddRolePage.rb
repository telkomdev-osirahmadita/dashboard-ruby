class AddRoleAdminPage < SitePrism::Page
    set_url("http://myptm-dashboard-dev.vsan-apps.playcourt.id/management-role/add")

    element :id_role_field, 'input[name="roleId"]'
    element :nama_role_field, 'input[name="roleName"]'
    element :tambah_role_button, 'button[type=submit]'
end