require 'capybara/cucumber'
require 'capybara/rspec'
require 'capybara-screenshot/cucumber'
require 'dotenv/load'
require 'faker'
require 'webdrivers'
require 'site_prism'

Capybara.default_driver = :selenium_chrome
Capybara.save_path = './screenshot'

Capybara.configure do |config|
    config.default_max_wait_time = 5
end

Before do
    Capybara.app_host = 'http://myptm-dashboard-dev.vsan-apps.playcourt.id/login'
    page.driver.browser.manage.window.resize_to(1366, 780)
end
