When(/^user login using confirmed user$/) do
    @browser = LoginPage.new
    @browser.load
    $LoginPage
    # @browser.username_field.set(ENV['USERNAME'])
    # @browser.password_field.set(ENV['PASSWORD'])
    # @browser.submit_button(wait: 20).click
    # sleep 2
end

And(/^user go to Tambah Role Admin page$/) do
    @browser = AddRoleAdminPage.new
    @browser.load
    sleep 2
end

And(/^user fill all field$/) do
    @browser = AddRoleAdminPage.new
    @browser.id_role_field.set(ENV['ID_ROLE_ADMIN'])
    @browser.nama_role_field.set(ENV['NAMA_ROLE_ADMIN'])
    @browser.tambah_role_button(wait: 20).click
    sleep 3
end