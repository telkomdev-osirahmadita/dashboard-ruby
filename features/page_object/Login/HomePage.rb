class HomePage < SitePrism::Page
    set_url("http://myptm-dashboard-dev.vsan-apps.playcourt.id/")
    
    element :username, '._2A7xFD7e0D_eIA_MgK30'
    element :data_customer_menu, 'a[href="/data-customer"]'
end