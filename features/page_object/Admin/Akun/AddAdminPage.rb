class AddAdminPage < SitePrism::Page
    set_url("http://myptm-dashboard-dev.vsan-apps.playcourt.id/management-admin/add")

    element :username_add_admin_field, 'input[name="username"]'
    element :namalengkap_field, 'input[name="fullName"]'
    element :email_field, 'input[name="email"]'
    element :roleadmin_dropdown, '._3R91X1TJZ6sJoB8fdA5V3x'
    element :roleadmin_value, '//div[@class="z3o9cKdBeYpdKHq2Zy1Xm"][@value="admin.cs"]'
    element :password_field, 'input[name=password]'
    element :confirm_password_field, 'input[name=password_confirmation]'
    element :tambah_admin_button, 'button[type=submit]'
end