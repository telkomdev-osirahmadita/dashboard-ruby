# When(/^user login using confirmed user$/) do
#     @browser = LoginPage.new
#     @browser.load
#     @browser.username_field.set(ENV['USERNAME'])
#     @browser.password_field.set(ENV['PASSWORD'])
#     @browser.submit_button(wait: 10).click
# end

And(/^user go to Data Customer Menu$/) do
    @browser = HomePage.new
    @browser.data_customer_menu(wait: 10).click
end

And(/^user click Download Report on this page$/) do
    @browser = DataCustomer.new
    page.execute_script "window.scrollBy(0,1000)"
    @browser.search_field.set(ENV['SEARCH_DATA_CUSTOMER'])
end