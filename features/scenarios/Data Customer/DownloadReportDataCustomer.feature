@downloadreport-datacustomer
Feature: Download Report Data Customer
Scenario: User can download report data customer at Data Customer Menu
    When user login using confirmed user
    And user go to Data Customer Menu
    And user click Download Report on this page
    Then user verify report successfully downloaded