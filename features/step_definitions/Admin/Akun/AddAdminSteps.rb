When(/^user login using confirmed user$/) do
    @browser = LoginPage.new
    @browser.load
    $LoginPage
    # @browser.username_field.set(ENV['USERNAME'])
    # @browser.password_field.set(ENV['PASSWORD'])
    # @browser.submit_button(wait: 20).click
    # sleep 2
end

And(/^user go to Tambah Admin page$/) do
    @browser = AddAdminPage.new
    @browser.load
    sleep 2
end

And(/^user fill all field$/) do
    @browser = AddAdminPage.new
    @browser.username_add_admin_field.set(ENV['USERNAME_TAMBAH_ADMIN'])
    @browser.namalengkap_field.set(ENV['NAMA_LENGKAP_TAMBAH_ADMIN'])
    @browser.email_field.set(ENV['EMAIL_TAMBAH_ADMIN'])
    @browser.roleadmin_dropdown(wait: 20).click
    scroll_to(@browser.tambah_admin_button)
    @browser.roleadmin_value(wait:20).click
    @browser.password_field.set(ENV['PASSWORD_TAMBAH_ADMIN'])
    @browser.confirm_password_field.set(ENV['KONFIRMASI_PASSWORD_TAMBAH_ADMIN'])
    sleep 3
end

