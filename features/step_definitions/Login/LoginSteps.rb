When(/^user is on MyPertamina Dashboard login page$/) do
    @browser = LoginPage.new
    @browser.load
    expect(@browser).to be_displayed
end

And(/^user input username at username field$/) do
    @browser = LoginPage.new
    @browser.username_field.set(ENV['USERNAME'])
end

And(/^user input password at password field$/) do
    @browser = LoginPage.new
    @browser.password_field.set(ENV['PASSWORD'])
end

And(/^user click masuk button$/) do
    @browser = LoginPage.new
    @browser.submit_button(wait: 10).click
end

Then(/^user successfully logged in as username at MyPertamina Dashboard$/) do
    @browser = HomePage.new
    expect(@browser).to be_displayed
    expect(@browser).to have_username
end