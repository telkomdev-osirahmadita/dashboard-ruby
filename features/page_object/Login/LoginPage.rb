class LoginPage < SitePrism::Page
    set_url("http://myptm-dashboard-dev.vsan-apps.playcourt.id/login")
    
    element :username_field, 'input[name="username"]'
    element :password_field, 'input[name="password"]'
    element :submit_button, 'button[type="submit"]'
end

def fill_username_field(username = ENV['USERNAME'])
    username_field.set(username_field)
    submit_button.click
end

def fill_password_field(password = ENV['PASSWORD'])
    password_field.set(password_field)
    submit_button.click
end

def login
    SitePrism::Waiter.wait_until_true(10) { !submit_button.disabled? }
    submit_button.click
end